<?php

namespace App\Controller;

use App\Model\User;
use Phalcon\Mvc\Controller as PhalconController;
use Phalcon\Mvc\View;

class BaseController extends PhalconController
{
    protected $user;

    protected $logined = false;

    public function initialize()
    {
        // Don't render a template for default
        $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
        $this->auth();
    }

    private function auth()
    {
        $uid = $this->session->get('uid');
        if (!empty($uid)) {
            $this->logined = true;
            $this->user = User::findFirst($uid);
            if (empty($this->user)) {
                $this->logined = false;
                $this->session->remove('uid');
            } else {
                $this->session->set('uid', $uid);
                $this->assign('uid', $uid);
                $this->assign('user', $this->user);

                if($this->user->role == User::banned) {
                    // $this->error('账号已被封禁');
                }
                $this->user->save();
            }
        }
        $this->assign('logined', $this->logined);
    }

    protected function assign($key, $value)
    {
        $this->view->setVar($key, $value);
    }

    protected function display()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);

        $compiler = $this->volt->getCompiler();
        $compiler->addFilter('url', function ($resolvedArgs, $exprArgs) {
            return '$this->url->get(' . $resolvedArgs . ')';
        });

        $this->assign('static', $this->url->get('/static/' . $this->config->site->theme));
    }

    protected function json($data)
    {
        $this->response->setContent(json_encode($data));
        $this->response->send();
        die;
    }

    protected function redirect($uri, array $params = [])
    {
        $url = $this->url->get($uri, $params);
        $this->response->redirect($url);
        $this->response->send();
        die;
    }

    protected function error($msg)
    {
        exit($msg);
    }

    protected function setMsg($msg)
    {
        $this->session->set('msg', $msg);
    }

    protected function getMsg()
    {
        if ($this->session->has('msg')) {
            $msg = $this->session->get('msg');
            $this->session->remove('msg');
            return $msg;
        } else {
            return null;
        }
    }

    protected function checkLogin($logined = true)
    {
        // 若 $logined == true, 则拒绝非登录用户操作, 反之则拒绝已登录用户操作
        if ($logined && !$this->logined) {
            $this->setMsg('请先登录');
            $this->redirect('/auth/login');
        } elseif (!$logined && $this->logined) {
            $this->redirect('/user');
        }
    }

    protected function genFormToken()
    {
        $formToken = '<input type="hidden" name="';
        $formToken .= $this->security->getTokenKey();
        $formToken .= '" value="';
        $formToken .= $this->security->getToken();
        $formToken .= '">';
        $this->assign('formToken', $formToken);
    }

    protected function checkFromToken()
    {
        if ($this->request->isPost()) {
            if (!$this->security->checkToken()) {
                exit('cracking?');
            }
        }
    }

}
