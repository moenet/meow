<?php

namespace App\Controller;

class HomeController extends BaseController
{
    public function indexAction()
    {
        $this->display();
    }
}
