<?php

namespace App\Controller;

use Phalcon\Security\Random;
use App\Model\InviteCode;
use App\Model\User;

class AuthController extends BaseController
{
    public function initialize()
    {
        parent::initialize();
        $this->checkLogin(false);
    }

    public function loginAction()
    {
        $this->genFormToken();
        $this->display();
    }

    public function handleLoginAction()
    {
        $this->checkFromToken();
    }

    public function regAction()
    {
        $this->genFormToken();
        $this->display();
    }

    public function handleRegAction()
    {
        $this->checkFromToken();

        $mail = $this->request->getPost('mail');
        if (!filter_var($mail, FILTER_VALIDATE_EMAIL)) {
            $this->error('邮箱格式不正确');
        }
        $existed = User::findFirstByMail($mail);
        if ($existed) {
            $this->error('已经有人用这个邮箱注册过了哦~');
        }

        $name = $this->request->getPost('name');
        if (empty($name)) {
            $this->error('用户名不能为空');
        }
        if (strlen($name) > 24) {
            $this->error('用户名太长了, 换一个小于25字的吧~');
        }
        // 3400-4dbf 中日韓統一漢字擴展集A
        // 4e00-9fbf 中日韓統一漢字
        // 3040-309f 平假名
        // 30a0-30ff 片假名
        if (!preg_match('/^[\x{3400}-\x{4dbf}\x{4e00}-\x{9fbf}\x{3040}-\x{309f}\x{30a0}-\x{30ff}A-Za-z0-9_\.\-]+$/u', $name)) {
            $this->error('用户名只能含有汉字、英文、数字、平假名、片假名以及下划线、减号、小数点');
        }
        $existed = User::findFirstByName($mail);
        if ($existed) {
            $this->error('已经有人用这个用户名注册过了哦~');
        }

        $pw = $this->request->getPost('pw');
        if ($pw != $this->request->getPost('pw2')) {
            $this->error('两次输入的密码不一致');
        }
        if (strlen($pw) < 6 || strlen($pw) > 20) {
            $this->error('密码必须大于6位且不超过20位');
        }
        if (!preg_match('/^[0-9A-Za-z\_\,\.\+\-\*\/\\\~\^\&\%\$\#\@\!\:\;\<\>\?]+$/', $pw)) {
            $this->error('密码只能包含数字、英文字母和这些符号: .,_+-*/\~^&%$#@!:;<>?');
        }

        $code = $this->request->getPost('code');
        $inviteCode = InviteCode::findFirstByCode($code);
        if (empty($inviteCode)) {
            $this->error('邀请码不存在');
        }

        $this->db->begin();

        $user = new User();
        $user->mail = $mail;
        $user->name = $name;
        $user->pw = $pw;
        $user->role = User::user;
        $user->reg_time = time();
        $user->inviter = $inviteCode->creator;

        $sid = User::getMaxSid();
        if (!$sid) {
            $sid = $this->config->app->min_sid;
        }
        $user->sid = $sid;
        $random = new Random();
        $user->spw = $random->base64Safe(8);

        if (!$user->save()) {
            $this->db->rollback();
            $this->error('注册失败, 未知错误');
        }

        if (!$inviteCode->delete()) {
            $this->db->rollback();
            $this->error('注册失败, 未知错误');
        }

        $this->redirect('/user');
    }

    public function logoutAction()
    {
        $this->session->destroy();
        $this->redirect('/auth/login');
    }
}
