<?php

namespace App;

use Phalcon\Loader as PhalconLoader;
use Phalcon\Config\Adapter\Ini as ConfigIni;
use Phalcon\Di\FactoryDefault;
use Phalcon\Mvc\Application;
use Phalcon\Mvc\View;
use Phalcon\Mvc\View\Engine\Volt;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\Url;
use Phalcon\Mvc\Router;
use Phalcon\Db\Adapter\Pdo\Mysql;
use Phalcon\Session\Adapter\Files as SessionFiles;
use Phalcon\Session\Adapter\Redis as SessionRedis;

class Loader
{
    protected $di;

    public function run()
    {
        // Create a DI
        $this->di = new FactoryDefault();
        try {
            $this->autoload();
            $this->config();
            $this->db();
            $this->session();
            $this->router();
            $this->view();
            $this->url();
            $this->dispatcher();

            // Handle the request
            $application = new Application($this->di);
            echo $application->handle()->getContent();
        } catch (\Exception $e) {
            echo "Exception: ", $e->getMessage();
        }
    }

    protected function autoload()
    {
        // Register Phalcon autoloader
        $loader = new PhalconLoader();
        $loader->registerNamespaces([
            'App\Controller' => APP_PATH . '/Controller/',
            'App\Console' => APP_PATH . '/Console/',
            'App\Model' => APP_PATH . '/Model/'
        ]);
        $loader->register();

        // Register Composer autoloader
        // require BASE_PATH . '/vendor/autoload.php';
    }

    protected function config()
    {
        // Setup the config
        $config = new ConfigIni(BASE_PATH . '/config.ini');
        $this->di->set('config', $config, true);
    }

    protected function db()
    {
        // Setup MySQL database
        $config = $this->di->get('config');
        $this->di->set('db', function () use ($config) {
            return new Mysql(array(
                'host' => $config->mysql->host,
                'username' => $config->mysql->username,
                'password' => $config->mysql->password,
                'dbname' => $config->mysql->database
            ));
        });
    }

    protected function session()
    {
        $config = $this->di->get('config');
        switch ($config->app->session) {
            case 'file':
                $session = new SessionFiles();
                break;
            case 'redis':
                $session = new SessionRedis([
                    'uniqueId' => 'meow',
                    'host' => $config->redis->host,
                    'port' => $config->redis->port,
                    'auth' => $config->redis->auth,
                    'lifetime' => 3600,
                    'prefix' => $config->redis->prefix
                ]);
                break;
        }
        $session->start();
        $this->di->set('session', $session, true);
    }

    protected function router()
    {
        $router = new Router();
        $router->setDefaults(array(
            'controller' => 'home',
            'action' => 'index'
        ));
        $router->handle();
        $this->di->set('router', $router, true);
    }

    protected function view()
    {
        // Setup the view component and Volt template engine
        $config = $this->di->get('config');
        $this->di->set('view', function () use ($config) {
            $view = new View();
            $view->setViewsDir(APP_PATH . '/View/' . $config->site->theme . '/');
            return $view;
        }, true);
        $volt = new Volt($this->di->get('view'), $this->di);
        $volt->setOptions([
            'compiledPath' => BASE_PATH . '/runtime/volt/',
            'compileAlways' => (bool) $config->app->debug
        ]);
        $this->di->set('volt', $volt, true);
        $this->di->get('view')->registerEngines([
            ".volt" => $volt
        ]);
    }

    protected function url()
    {
        // Setup a base URI so that all generated URIs include the "tutorial" folder
        $config = $this->di->get('config');
        $this->di->set('url', function () use ($config) {
            $url = new Url();
            $url->setBaseUri($config->app->baseuri);
            return $url;
        }, true);
    }

    protected function dispatcher()
    {
        // Setup the namespace of Dispatcher
        $this->di->set('dispatcher', function () {
            $this->dispatcher = new Dispatcher();
            $this->dispatcher->setDefaultNamespace('App\Controller');
            return $this->dispatcher;
        }, true);
    }
}
