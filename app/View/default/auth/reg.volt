{% extends "auth/layout.volt" %}

{% block title %}注册{% endblock %}

{% block form %}
    <p class="login-box-msg">注册</p>
    <form action="{{ '/auth/handleReg' | url }}" method="post">
        {{ formToken }}
        <div class="form-group has-feedback">
            <input type="text" name="mail" class="form-control" placeholder="邮箱">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <input type="text" name="name" class="form-control" placeholder="用户名">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <input type="password" name="pw" class="form-control" placeholder="密码">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <input type="password" name="pw2" class="form-control" placeholder="再次输入密码">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <input type="text" name="code" class="form-control" placeholder="邀请码">
            <span class="glyphicon glyphicon-barcode form-control-feedback"></span>
        </div>
        <div class="row">
            <div class="col-xs-4 col-xs-offset-8">
                <button type="submit" class="btn btn-primary btn-block btn-flat">注册</button>
            </div>
        </div>
    </form>

    <br>
    <a href="{{ '/auth/login' | url }}">登录</a>
    <a href="{{ 'passwd' | url }}" class="pull-right">忘记密码</a>
{% endblock %}
