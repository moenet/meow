{% extends "auth/layout.volt" %}

{% block title %}登录{% endblock %}

{% block form %}
    <p class="login-box-msg">登录</p>
    <form action="{{ '/auth/handleLogin' | url }}" method="post">
        {{ formToken }}
        <div class="form-group has-feedback">
            <input type="text" name="name" class="form-control" placeholder="用户名或邮箱">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <input type="password" name="pw" class="form-control" placeholder="密码">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div class="row">
            <div class="col-xs-8">
                <div class="checkbox icheck">
                    <label>
                        <input type="checkbox" name="rem" value="1"> 记住我
                    </label>
                </div>
            </div>
            <!-- /.col -->
            <div class="col-xs-4">
                <button type="submit" class="btn btn-primary btn-block btn-flat">登录</button>
            </div>
            <!-- /.col -->
        </div>
    </form>

    <br>
    <a href="{{ '/auth/reg' | url }}">注册</a>
    <a href="{{ '/auth/passwd' | url }}" class="pull-right">忘记密码</a>
{% endblock %}
