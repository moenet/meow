{% extends "home/layout.volt" %}

{% block title %}Index Action{% endblock %}

{% block content %}
    <section class="container description">
        <spann class="icon">
            <i class="fa fa-paper-plane" aria-hidden="true"></i>
        </spann>
        <h1>{{ config.site.title }}</h1>
        <h4 class="text-muted">{{ config.site.msg }}</h4>
        {% if logined %}
            <a class="btn btn-primary btn-lg btn-flat index-action" href="{{ '/user' | url }}">进入用户中心</a>
        {% else %}
            <a class="btn btn-primary btn-lg btn-flat index-action" href="{{ '/auth/reg' | url }}">立即注册</a>
        {% endif %}
    </section>

    <div class="container grid">
        <div class="row">
            <div class="col-md-4">
                <div class="icon-block">
                    <span><i class="fa fa-flash" aria-hidden="true"></i></span>
                    <h3>Super Fast</h3>
                    <p class="text-muted">
                        Bleeding edge techniques using Asynchronous I/O and Event-driven programming.
                    </p>
                </div>
            </div>

            <div class="col-md-4">
                <div class="icon-block">
                    <span><i class="fa fa-group" aria-hidden="true"></i></span>
                    <h3>Open Source</h3>
                    <p class="text-muted">
                        Totally free and open source. A worldwide community devoted to deliver bug-free code and long-term support.
                    </p>
                </div>
            </div>

            <div class="col-md-4">
                <div class="icon-block">
                    <span><i class="fa fa-cog" aria-hidden="true"></i></span>
                    <h3>Easy to work with</h3>
                    <p class="text-muted">
                        Avaliable on multiple platforms, including PC, MAC, Mobile (Android and iOS) and Routers (OpenWRT).
                    </p>
                </div>
            </div>
        </div>
    </div>

{% endblock %}
