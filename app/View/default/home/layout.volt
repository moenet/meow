<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">

    <meta name="generator" content="Meow">
    <meta name="keywords" content="{{ config.meta.keywords }}">
    <meta name="description" content="{{ config.meta.description }}">
    <link href="{{ static }}/favicon.ico" rel="shortcut icon">

    <title>{% block title %}{% endblock %} - {{ config.site.name }}</title>

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="{{ static }}/adminlte/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ static }}/fonts/font-awesome.min.css">

    <!-- Overwrite Framework Styles -->
    <link rel="stylesheet" href="{{ static }}/css/index.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- REQUIRED JS SCRIPTS -->

    <!-- jQuery 2.1.4 -->
    <script src="{{ static }}/adminlte/plugins/jQuery/jQuery-2.2.0.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="{{ static }}/adminlte/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<nav class="navbar navbar-static-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nav-menu" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">{{ config.site.name }}</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="nav-menu">
            <ul class="nav navbar-nav">
                <li class="active"><a href="{{ '/' | url }}">首页</a></li>
                <li><a href="{{ 'code' | url }}">邀请码</a></li>
                <li><a href="https://shadowsocks.org/en/download/clients.html" target="_blank">客户端</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                {% if logined %}
                    <li><a href="{{ '/user' | url }}">用户中心</a></li>
                    <li><a href="{{ '/auth/logout' | url }}">登出</a></li>
                {% else %}
                    <li><a href="{{ '/auth/login' | url }}">登录</a></li>
                    <li><a href="{{ '/auth/reg' | url }}">注册</a></li>
                {% endif %}
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container -->
</nav>

{% block content %}
{% endblock %}

<footer>
    <div class="container">
        <span>Powered by Meow.</span>
        <span><a href="{{ '/tos' | url }}">Terms of service</a></span>
    </div>
</footer>

</body>
</html>
