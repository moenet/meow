<?php

namespace App\Model;

use Phalcon\Mvc\Model;

class User extends Model
{
    const visitor    = 0;  //游客
    const banned     = 1;  //被封禁用户
    const user       = 2;  //用户
    const admin      = 3;  //管理员
    const novitiate  = 4;  //见习中用户
    const unverified = 5;  //未验证邮箱

    public function verifyPw($pw)
    {
        return password_verify($pw, $this->pw);
    }

    public static function encodePw($pw, $cost = 5)
    {
        return password_hash($pw, PASSWORD_DEFAULT, ['cost' => $cost]);
    }

    public static function getMaxSid()
    {
        $biggest = self::query()->order('sid desc')->execute();
        if (empty($biggest)) {
            return null;
        }
        return $biggest->sid;
    }
}
