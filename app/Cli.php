<?php

namespace App;

use Phalcon\Di\FactoryDefault\Cli as CliDi;
use Phalcon\Cli\Console;
use Phalcon\Cli\Dispatcher;

class Cli extends Loader
{
    public function run()
    {
        global $argv;

        // Using the CLI factory default services container
        $this->di = new CliDi();
        try {
            $this->autoload();
            $this->config();
            $this->db();
            $this->dispatcher();

            // Create a console application
            $console = new Console();
            $console->setDI($this->di);

            // Process the console arguments
            $arguments = array();
            foreach ($argv as $k => $arg) {
                if ($k == 1) {
                    $arguments['task'] = $arg;
                } elseif ($k == 2) {
                    $arguments['action'] = $arg;
                } elseif ($k >= 3) {
                    $arguments['params'][] = $arg;
                }
            }

            // Define global constants for the current task and action
            define('CURRENT_TASK',   (isset($argv[1]) ? $argv[1] : null));
            define('CURRENT_ACTION', (isset($argv[2]) ? $argv[2] : null));

            // Handle incoming arguments
            $console->handle($arguments);
        } catch (\Exception $e) {
            echo "Exception: " . $e->getMessage() . "\n";
        }
    }

    protected function dispatcher()
    {
        // Setup the namespace of Dispatcher
        $this->di->set('dispatcher', function () {
            $this->dispatcher = new Dispatcher();
            $this->dispatcher->setDefaultNamespace('App\Console');
            return $this->dispatcher;
        }, true);
    }
}