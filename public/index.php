<?php

if (version_compare(PHP_VERSION, '5.6.0', '<')) {
    die('Require PHP > 5.6.0!');
}
if (!extension_loaded('phalcon')) {
    die('Require phalcon extension!');
}

define('BASE_PATH', __DIR__ . '/..');
define('APP_PATH', BASE_PATH . '/app');

require APP_PATH . '/Loader.php';

$app = new \App\Loader();
$app->run();
