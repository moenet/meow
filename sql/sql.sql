CREATE TABLE IF NOT EXISTS `user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  `pw` char(60) NOT NULL,
  `mail` varchar(50) NOT NULL,
  `role` tinyint(2) NOT NULL DEFAULT '2',
  `reg_time` int(10) unsigned NOT NULL DEFAULT '0',
  `last_login` int(10) unsigned NOT NULL DEFAULT '0',
  `ip` varchar(120) DEFAULT '',
  `money` int(10) unsigned NOT NULL DEFAULT '0',
  `invite_limit` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `inviter` int(10) unsigned DEFAULT NULL,

  `sid` int(10) unsigned NOT NULL COMMENT '服务ID，用作SS端口',
  `spw` char(20) NOT NULL COMMENT '服务密码，用作SS密码，明文',

  PRIMARY KEY (`uid`),
  UNIQUE INDEX `name` (`name`) USING BTREE,
  UNIQUE INDEX `mail` (`mail`) USING BTREE,
  UNIQUE INDEX `sid` (`sid`) USING BTREE
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_general_ci
ROW_FORMAT=COMPACT
AUTO_INCREMENT=1;


CREATE TABLE IF NOT EXISTS `invite_code` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(64) NOT NULL,
  `create_time` int(10) unsigned NOT NULL DEFAULT '0',
  `creator` int(10) unsigned NOT NULL DEFAULT '0',

  PRIMARY KEY (`id`),
  UNIQUE INDEX `code` (`code`) USING BTREE
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_general_ci
ROW_FORMAT=COMPACT;


CREATE TABLE IF NOT EXISTS `option` (
  `name` varchar(32) NOT NULL,
  `value` varchar(240),

  PRIMARY KEY (`name`)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_general_ci
ROW_FORMAT=COMPACT;
