# Meow

A Shadowsocks manyusers dashboard based on [Phalcon](https://github.com/phalcon/cphalcon).

## Requirement

* PHP 5.4 or later with Phalcon extension
* MySQL

## Installation

See [wiki](https://github.com/balthild/meow/wiki/Installation).

## License

GNU General Public License v3

[License text](LICENSE)
